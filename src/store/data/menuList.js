export default {
  data: [
    {
      icon: 'account_circle',
      label: 'User',
      color: 'blue',
      link: '/userinfo',
      separator: false
    },
    {
        icon: 'settings',
        label: 'Settings',
        color: 'blue-grey-7',
        link: '/settings',
        separator: true
      },
    {
        icon: 'help',
        label: 'Privacy',
        color: 'accent',
        link: '/privacy',
        separator: false
    },
    {
      icon: 'supervisor_account',
      label: 'Contact Us',
      color: 'red-6',
      link: '/ContactUs',
      separator: false
    },
    {
      icon: 'feedback',
      label: 'Send Feedback',
      color: 'orange',
      link: '/sendfeedback',
      separator: true
    },
    {
    icon: 'logout',
    label: 'Logout',
    color: 'black',
    link: '',
    separator: false
    },
  ]
}